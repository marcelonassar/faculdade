Rails.application.routes.draw do
  resources :users
  
  devise_for :users
  root to: 'pages#home'

  
  resources :students
  resources :lectures
  resources :classrooms
  resources :semesters
  resources :courses
  resources :subjects
  resources :teachers
  resources :departments
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
