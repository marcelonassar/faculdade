class Subject < ApplicationRecord
  belongs_to :department, optional: true

  has_many :equivalences
  has_many :equivalent_disciplines, through: :equivalences, source: :discipline

  has_many :subject_courses
  has_many :courses, through: :subject_courses
end
