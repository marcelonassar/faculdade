class Classroom < ApplicationRecord
  belongs_to :teacher
  belongs_to :semester
  belongs_to :subject
  has_many :lectures
  
  has_many :classroom_students
  has_many :students, through: :classroom_students
end
