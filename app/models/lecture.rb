class Lecture < ApplicationRecord
  belongs_to :classroom
  has_many :lecture_students
  has_many :students, through: :lecture_students
end
