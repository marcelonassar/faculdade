class Student < ApplicationRecord
  belongs_to :course
  has_many :lecture_students
  has_many :lectures, through: :lecture_students

  has_many :classroom_students
  has_many :classrooms, thourgh: :classroom_students
end
