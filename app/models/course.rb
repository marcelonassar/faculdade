class Course < ApplicationRecord
  belongs_to :department
  has_many :subject_courses
  has_many :students, through: :subject_courses
end
