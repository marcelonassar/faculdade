class Department < ApplicationRecord
    has_many :courses
    has_many :teachers
    has_many :subjects
end
