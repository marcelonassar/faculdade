json.extract! classroom, :id, :teacher_id, :semester_id, :subject_id, :name, :created_at, :updated_at
json.url classroom_url(classroom, format: :json)
