json.extract! semester, :id, :year, :second, :created_at, :updated_at
json.url semester_url(semester, format: :json)
