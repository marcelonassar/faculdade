json.extract! lecture, :id, :time, :classroom_id, :created_at, :updated_at
json.url lecture_url(lecture, format: :json)
