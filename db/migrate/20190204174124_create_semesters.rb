class CreateSemesters < ActiveRecord::Migration[5.2]
  def change
    create_table :semesters do |t|
      t.integer :year
      t.boolean :second

      t.timestamps
    end
  end
end
