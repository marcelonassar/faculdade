class CreateClassrooms < ActiveRecord::Migration[5.2]
  def change
    create_table :classrooms do |t|
      t.references :teacher, foreign_key: true
      t.references :semester, foreign_key: true
      t.references :subject, foreign_key: true
      t.string :name

      t.timestamps
    end
  end
end
